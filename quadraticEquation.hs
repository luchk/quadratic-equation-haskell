root a b c = if d < 0 then error "There are no roots" -- main function
                  else (if d == 0 then x else x , y)
                        where
                          x = e - sqrt d / (2 * a) -- first x
                          y = e + sqrt d / (2 * a) -- second x
                          d = b * b - 4 * a * c    -- discriminant
                          e = - b / (2 * a)        -- part of discriminant