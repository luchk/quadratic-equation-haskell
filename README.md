# Quadratic equation

### How does it look?

a*x^2+b*x+c=0

### How does discriminant looks?

D = b^2 - 4*a*c

### How to calculate first and second x?

You should use this formula
![alt text](https://wikimedia.org/api/rest_v1/media/math/render/svg/a0252e1df0e4d27a338a3725eccc16e5b803c11d "Logo Title Text 1")

### How to youse this Haskell program?

+ Download this  [quadraticEquation.hs](https://gitlab.com/luchk/quadratic-equation-haskell/blob/master/quadraticEquation.hs) file
+ Open your terminal
+ Open GHCI
+ Load this file
+ Type root 4 5 (-10) and get
(-2.3251838135919307,1.0751838135919305)